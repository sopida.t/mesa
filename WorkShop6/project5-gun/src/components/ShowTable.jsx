import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import { useState, useEffect } from "react";
import { ButtonGroup } from "@mui/material";
import Button from "@mui/material/Button";

export default function ShowTable() {
  const [items, setItems] = useState([]);
  useEffect(() => {
    UserGet();
  }, []);

  const UserGet = () => {
    fetch("https://4ef6-202-28-119-38.ngrok-free.app/customer")
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  };

  const DeleteCustomer = (id) => {
    var raw = "";
    var requestOptions = {
      method: "DELETE",
      body: raw,
      redirect: "follow",
    };

    fetch(
      "https://4ef6-202-28-119-38.ngrok-free.app/customer/" + id,
      requestOptions
    )
      .then((response) => response.text())
      .then(() => {
        UserGet();
      })
      .catch((error) => console.log("error", error));
  };

  const EditCustomer = (id) => {
    console.log("go to edit " + id);
    window.location = "/Edit/" + id;
  };

  return (
    <>
      <React.Fragment>
        <CssBaseline />
        <Container maxWidth="lg" sx={{ p: 2 }}>
          <Paper sx={{ p: 2 }}>
            <Box display="flex">
              <Box sx={{ flexGrow: 1 }}>
                <Typography variant="h6" gutterBottom>
                  Customers
                </Typography>
              </Box>
            </Box>

            <TableContainer component={Paper}>
              <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                  <TableRow>
                    <TableCell>ID</TableCell>
                    <TableCell align="right">Name</TableCell>
                    <TableCell align="right">Phone Number</TableCell>
                    <TableCell align="right">Action</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {items.map((row) => (
                    <TableRow
                      key={row.customer_id}
                      sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                    >
                      <TableCell component="th" scope="row">
                        {row.customer_id}
                      </TableCell>
                      <TableCell align="right">{row.customer_name}</TableCell>
                      <TableCell align="right">{row.phone_number}</TableCell>
                      <TableCell align="right">
                        <ButtonGroup
                          variant="outlined"
                          aria-label="outlined button group"
                        >
                          <Button
                            onClick={() => EditCustomer(row.customer_id)}
                            color="primary"
                            disabled={false}
                            size="medium"
                            variant="elevated"
                            sx={{ margin: 2 }}
                          >
                            Edit
                          </Button>
                          <Button
                            onClick={() => DeleteCustomer(row.customer_id)}
                            color="primary"
                            disabled={false}
                            size="medium"
                            variant="elevated"
                            sx={{ margin: 2 }}
                          >
                            Delete
                          </Button>
                        </ButtonGroup>
                      </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </TableContainer>
          </Paper>
        </Container>
      </React.Fragment>
    </>
  );
}
