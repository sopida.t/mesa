import Navbar from "./components/Navbar";
import { Routes, Route, Navigate } from "react-router-dom";
import "./App.css";
import Cards from "./components/Cards";
import ShowTable from "./components/ShowTable";
import CreateCustomer from "./components/CreateCustomer";
import Notfound from "./components/Notfound";
import EditCustomer from "./components/EditCustomer"

function App() {
  
  return (
    <div className="App">
      
        <Navbar />
        <Routes>
          <Route path="/" element={<ShowTable />} />
          <Route path="/Cards" element={<Cards />} />
          <Route path="/Create" element={<CreateCustomer />} />
          <Route path="/Edit/:id" element={<EditCustomer />} />
          <Route path="/Table" element={<Navigate to="/"/>}></Route>
          <Route path="*" element={<Notfound/>}></Route>
        </Routes>
    
    </div>
  );
}

export default App;
