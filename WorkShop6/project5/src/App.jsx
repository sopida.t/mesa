import "./App.css";
import Navbar from "./Navbar";
import CustomersTable from "./CustomersTable";
import { Routes, Route, BrowserRouter } from "react-router-dom";
import UserCreate from "./UserCreate";
import UserUpdate from "./UserUpdate";
import CustomersCard from "./CustomersCard";


function App() {
  return (
    <div>
      <Navbar />
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<CustomersTable />} />
          <Route path="/Card" element={<CustomersCard />} />
          <Route path="create" element={<UserCreate />} />
          <Route path="update/:id" element={<UserUpdate/>}/>
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
