import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import { TextField, Typography } from "@mui/material";
import Grid from "@mui/material/Grid";
import Button from "@mui/material/Button";
import { useState } from "react";
import "./UserCreate.css"

const urlApi = "https://735c-110-170-188-98.ngrok-free.app/customer";

export default function UserCreate() {
  const handleSubmit = (event) => {
    console.log("HandleSubmit is runing");
    event.preventDefault();
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      Customer_name: name,
      Phone_number: tel,
      Birth_date: birthDate,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    fetch(urlApi, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log(result);
        alert(result);
        if (result === "Insert Customer Complete") {
          window.location.href = "/";
        }
      })
      .catch((error) => console.log("error", error));
  };
  const [name, setName] = useState("");
  const [tel, setTel] = useState("");
  const [birthDate, setBirthDate] = useState("");

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom component="div">
          Create User
        </Typography>
        <form onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                id="name"
                label="Name"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="tel"
                label="Phone Number"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setTel(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                id="birthDate"
                label="Birth Date"
                variant="outlined"
                fullWidth
                required
                onChange={(e) => setBirthDate(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <Button type="submit" variant="contained" fullWidth>
                Create
              </Button>
            </Grid>
          </Grid>
        </form>
      </Container>
    </React.Fragment>
  );
}
