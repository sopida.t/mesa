import React, { useContext } from "react";
import { studentsContext } from "../App";
import "../components/Item.css";
const Item = ({ data }) => {
  //หรือจะรับแค่ตัวแปร {name, id} ก็ได้แต่ต้องเรียกใช้แยก
  const { students, setStudent } = useContext(studentsContext);
  const deleteStudent = (id) => {
    setStudent(students.filter((data) => data.id !== id));
  };
  return (
    <>
      <li key={data.id} className={data.gender}>
        <h1>
          {data.name} {data.gender}
        </h1>
        {/* หรือจะกำหนดเป็น inline style ก็ได้ */}
        {/* style={{
          backgroundColor: data.gender === "male" ? "red" : "darkcyan",
          color: "white",
        }} */}
        <button
          onClick={() => deleteStudent(data.id)}
          className="delete"
          style={{ backgroundColor: "black" }}
        >
          ลบ
        </button>
      </li>
    </>
  );
};

export default Item;
