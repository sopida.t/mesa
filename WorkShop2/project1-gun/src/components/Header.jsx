import "./Header.css";
import logo from "../profile.jfif";
export default function Header(props) {
    const {theme, setTheme} = props;
    function toggleTheme(){
        if(theme==="day"){
            setTheme("night")
        }else{
            setTheme("day")
        }
    }
  return (
    <>
      <nav>
        <div>
          <img src={logo} alt="logo" className="logo" />
        </div>
        <ul className="link">
          <li>
            <a href="/">Home</a>
          </li>
          <li>
            <a href="/">StudentList</a>
          </li>
          <li>
            <a href="/">About</a>
          </li>
          <li className="theme-container">
            <span onClick={toggleTheme}>{theme==="day" ? "day":"night"}mode</span>
          </li>
        </ul>
      </nav>
    </>
  );
}
