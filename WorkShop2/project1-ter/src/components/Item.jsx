import "./Item.css"
import { BiTrashAlt } from "react-icons/bi";
import { BiEditAlt } from "react-icons/bi";
export default function Item(props){
    const {data,deleteTask, updateTask} = props
    return(
        <>
        <div className="list">
            <p className="title">{data.title}</p>
            <div className="btn-container">
                <BiTrashAlt className="btn" onClick={()=>deleteTask(data.id)}/>
                <BiEditAlt className="btn" onClick={()=>updateTask(data.id)}/>
            </div>
        </div>
        </>
    )
}