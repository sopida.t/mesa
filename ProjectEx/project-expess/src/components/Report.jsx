import { Box, Button, Typography } from "@mui/material";
import "../CSS/Report.css";
import Profile from "../Images/profile.jfif";
import StarIcon from "@mui/icons-material/Star";
import StarBorderIcon from "@mui/icons-material/StarBorder";
import { Link } from "react-router-dom";
export default function Report() {
  return (
    <>
      <Box className="container-report">
        <Box className="header" />
        <Box className="container-rider">
          <img src={Profile} alt="profile" />
          <Typography sx={{ fontSize: "2vh" }}>ผู้ส่ง: ดีโอ บลันโด</Typography>
          <Typography sx={{ fontSize: "2vh" }}>
            โปรดให้คะแนนความพึงพอใจผู้ส่ง
          </Typography>
          <Box>
            <StarIcon sx={{ p: 1, fontSize: "4vh" }} />
            <StarIcon sx={{ p: 1, fontSize: "4vh" }} />
            <StarIcon sx={{ p: 1, fontSize: "4vh" }} />
            <StarIcon sx={{ p: 1, fontSize: "4vh" }} />
            <StarBorderIcon sx={{ p: 1, fontSize: "4vh" }} />
          </Box>
          <Typography sx={{ textDecoration: "underline" }}>
            บันทึกไรเดอร์คนนี้
          </Typography>
        </Box>
        <Box className="container-total">
          <Box>
            <Typography sx={{ fontSize: "1.2rem" }}>Total</Typography>
          </Box>
          <Box>
            <Typography sx={{ fontSize: "1.2rem" }}>฿750.00</Typography>
            <Typography sx={{ color: "#2e2e2e" }}>04xx</Typography>
          </Box>
        </Box>
        <Box className="container-route">
          <Typography sx={{ fontSize: "2rem", fontWeight: "bold" }}>
            เส้นทาง
          </Typography>
          <Typography sx={{ fontWeight: "bold" }}>จุดเริ่ม</Typography>
          <Typography>
            Lao Peng Nguan Tower 1(LPN) แขวงจอมพล เขตจตุจักร กรุงเทพมหานคร
          </Typography>
          <Typography sx={{ fontWeight: "bold" }}>จุดที่ 2</Typography>
          <Typography>
            992 ถ. พหลโยธิน แขวงจอมพล เขตจตุจักร กรุงเทพมหานคร 10110
          </Typography>
          <Typography sx={{ fontWeight: "bold" }}>จุดที่ 3</Typography>
          <Typography>
            อ่อนนุช 21/1 ถ.สุขุมวิท แขวงสวนหลวง เขตสวนหลวง
          </Typography>
        </Box>
        <Box className="container-linkbutton">
          <Link to="/" >
            <Button variant="contained">Contained</Button>
          </Link>
        </Box>
      </Box>
    </>
  );
}
