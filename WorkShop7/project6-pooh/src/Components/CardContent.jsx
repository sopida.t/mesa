import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Whatshot from "@mui/icons-material/Whatshot";
import Chip from "@mui/material/Chip";
import "./CC.css";

export default function CardContent() {
  return (
    <React.Fragment>
      <CssBaseline />
      <Box
        className="card"
        sx={{
          bgcolor: "#FFFFFF",
          height: "auto",
          borderRadius: "1.5rem",
          marginLeft: "20px",
          marginRight: "20px",
          marginTop: "5px",
          padding: "15px",
          boxShadow: "2px 2px 5px #888888",
        }}
      >
        <Box
          sx={{
            display: "flex",
            justifyContent: "space-between",
            justifyItems: "space-evenly",
            margin: "5px",
          }}
        >
          <Typography
            variant="h1"
            gutterBottom
            sx={{ fontSize: "15px", fontWeight: "bold", color: "#5E5D6A" }}
          >
            Villa for 16 guests in Ubud
          </Typography>
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              justifyItems: "space-evenly",
              minWidth: "10px",
              minHeight: "10px",
            }}
          >
            <Whatshot sx={{ marginTop: "-5px", color: "#FAC336" }} />
            <Typography
              variant="h1"
              gutterBottom
              sx={{ fontSize: "15px", fontWeight: "bold", color: "#756C4F" }}
            >
              3 offers
            </Typography>
          </Box>
        </Box>
        <Box
          sx={{
            margin: "5px",
            marginTop:"-5px",
            paddingBottom: "20px",
            fontSize: "12px",
          }}
        >
          <Typography
            sx={{ fontSize: "14px", fontWeight: "bold", color: "#B6B4BC" }}
          >
            Dec 20, 2022 - Jan 4, 2022
          </Typography>
        </Box>
        <Box sx={{ flexGrow: 1, display: "inline-box",marginTop:"-15px",padding:"0"}}>
          
            <Chip sx={{margin:"5px",fontSize:"11px",fontWeight:"bold",color:"white",backgroundColor:"#45C9B2"}} label="16 guests" />
            <Chip sx={{margin:"5px",fontSize:"11px",fontWeight:"bold",color:"white",backgroundColor:"#6546F5"}} label="5 bedrooms" variant="outlined" />
            <Chip sx={{margin:"5px",fontSize:"11px",fontWeight:"bold",color:"white",backgroundColor:"#76CF4A"}} label="$14000 - $18000" />
            <Chip sx={{margin:"5px",fontSize:"11px",fontWeight:"bold",color:"white",backgroundColor:"#478AF3"}} label="open pool" />
            <Chip sx={{margin:"5px",fontSize:"11px",fontWeight:"bold",color:"white",backgroundColor:"#F26645"}} label="kitchen" />
            <Chip sx={{margin:"5px",fontSize:"11px",fontWeight:"bold",color:"white",backgroundColor:"#BA49F1"}} label="Wi-Fi" />
          
        </Box>
      </Box>
    </React.Fragment>
  );
}
