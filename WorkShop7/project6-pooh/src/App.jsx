import "./App.css";
import * as React from "react";
import Box from "@mui/material/Box";
import CardContent from "./Components/CardContent";
import AddIcon from "@mui/icons-material/Add";
import BottomNavigate from "./Components/BottomNavigator";
import SuggestCard from "./Components/SuggestCard";
import Typography from "@mui/material/Typography";
import "./App.css";
import Link from "@mui/material/Link";
import Fab from "@mui/material/Fab";
import BottomNavigation from "@mui/material/BottomNavigation";
import BottomNavigationAction from "@mui/material/BottomNavigationAction";

import SettingsIcon from "@mui/icons-material/Settings";
import Paper from "@mui/material/Paper";

import BookmarksIcon from "@mui/icons-material/Bookmarks";
import ForumIcon from "@mui/icons-material/Forum";
import ExploreIcon from "@mui/icons-material/Explore";
import AppsIcon from "@mui/icons-material/Apps";
import CssBaseline from "@mui/material/CssBaseline";

function App() {
  const [value, setValue] = React.useState(0);

  return (
    <React.Fragment>
      <Box
        className="outside"
        sx={{
          backgroundColor: "#d8d8d8",
          padding: "5px",
          width: "fit-content",
          height: "auto",
          maxWidth: "1280px",
        }}
      >
        <Box
          className="App"
          sx={{
            paddingTop: "12%",

            margin: "auto",
            backgroundColor: "#F9FAFF",
            borderRadius: "3.5rem",
            paddingBottom: "50px",
            width: "fit-content",
            height:"fit-content",
            minWidth: "300px",
            minHeight: "300px",
            maxWidth: "1280px",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "space-between",
              justifyItems: "space-evenly",
              padding: "20px",
            }}
          >
            <Typography
              sx={{ fontWeight: "bold", fontSize: "5vh", color: "#4C4C62" }}
            >
              Applications
            </Typography>
            <Fab
              size="small"
              aria-label="add"
              sx={{ marginTop: "8px", backgroundColor: "#FFCB47" }}
            >
              <AddIcon color="dark" />
            </Fab>
          </Box>
          <Box
            sx={{
              marginTop: "-10px",
              display: "inline-block",
              paddingTop: "10px",
              paddingBottom: "10px",
              marginRight: "100px",
              marginLeft: "2.5%",
            }}
          >
            <Link
              href=""
              sx={{
                textDecoration: "none",
                color: "#4E4C66",
                fontSize: "5vw",
                fontWeight: "bold",
                padding: "0 -10",
                margin: "10px",
              }}
            >
              Current
            </Link>
            <Link
              href=""
              sx={{
                textDecoration: "none",
                color: "#9193A0",
                fontSize: "5vw",
                fontWeight: "bold",
                padding: "0 2px",
                margin: "10px",
              }}
            >
              Executed
            </Link>
            <Link
              href=""
              sx={{
                textDecoration: "none",
                color: "#9193A0",
                fontSize: "5vw",
                fontWeight: "bold",
                padding: "0 2px",
                margin: "10px",
              }}
            >
              All
            </Link>
          </Box>
          <CardContent />
          <SuggestCard />
        </Box>

        <BottomNavigate />
      </Box>
    </React.Fragment>
  );
}

export default App;
