import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Header from './components/Header'
import Navbar from './components/Navbar'
import Details from './components/Details'
import { Route, Routes } from 'react-router-dom'

function App() {

  return (
    <div>
      <Navbar/>
      <Routes>
        <Route path="/" element={<Header/>} />
        <Route path="details" element={<Details/>} />
      </Routes>
      {/* <Details /> */}
      {/* <CardCarousel/> */}
    </div>
    
  )
}

export default App