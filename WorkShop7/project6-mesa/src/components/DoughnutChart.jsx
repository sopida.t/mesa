import React, { useEffect, useRef } from "react";
import { Doughnut } from "react-chartjs-2";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";

ChartJS.register(ArcElement, Tooltip, Legend);

export default function DoughnutChart() {
  const chartRef = useRef(null);
  const spentAmount = 760;

  useEffect(() => {
    if (chartRef && chartRef.current) {
      const chartInstance = chartRef.current.chartInstance;
      if (chartInstance) {
        chartInstance.destroy();
      }
    }
  }, [spentAmount]);

  const data = {
    datasets: [
      {
        data: [760, 1000],
        backgroundColor: ["#2b72f7", "#d4ddfc"],
        borderWidth: 0,
      },
    ],
  };

  const options = {
    cutout: "60%",
    responsive: true,
    maintainAspectRatio: false,
    plugins: {
      tooltip: {
        enabled: false,
      },
    },
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container >
        <Box display={'flex'} maxWidth={'100%'}>
          <Doughnut data={data} options={options} ref={chartRef} />
        </Box>
      </Container>
    </React.Fragment>
  );
}
