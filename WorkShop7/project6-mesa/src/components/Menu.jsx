import * as React from "react";
import "./Homepage.css";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import "./Detail.css";
import Grid from "@mui/material/Grid";
import StarRoundedIcon from "@mui/icons-material/StarRounded";
import RestaurantRoundedIcon from '@mui/icons-material/RestaurantRounded';
import ShoppingBagRoundedIcon from '@mui/icons-material/ShoppingBagRounded';
import IconButton from "@mui/material/IconButton";
import { useTheme } from "@mui/material/styles";

export default function Homepage() {
  const theme = useTheme();

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="100%">
        <Box sx={{ flexGrow: 1 }}>
          <Grid
            container
            display="flex"
            flexWrap="nowrap"
            direction="row"
            alignItems="center"
            mx="auto"
            sx={{
            [theme.breakpoints.up("md")]: {
              
            },
            [theme.breakpoints.up("sm")]: {
              justifyContent:'space-evenly',
            },
            [theme.breakpoints.up("xs")]: {
              justifyContent:'space-around'
            },
          }}
          >
            <Grid
              alignItems="center"
              sx={{ display: "inline-flex", flexDirection: "column" }}
            >
              <IconButton
                sx={{
                  p: 2,
                  fontSize: "4rem",
                  background: "#d4ddfc",
                  borderRadius: '50%',
                }}
              >
                <Typography sx={{ width:25,color: "#222", fontWeight:'bold' }} >All</Typography>
              </IconButton>
              <Typography
                sx={{ fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" } }}
              >
                All
              </Typography>
            </Grid>
            <Grid
              alignItems="center"
              sx={{ display: "inline-flex", flexDirection: "column" }}
            >
              <IconButton
                sx={{
                  p: 2,
                  fontSize: "3.5rem",
                  background: "#d4ddfc",
                  borderRadius: '50%',
                }}
              >
                <StarRoundedIcon sx={{ color: "#222" }} />
              </IconButton>
              <Typography
                sx={{ fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" } }}
              >
                Entertainment
              </Typography>
            </Grid>
            <Grid
              alignItems="center"
              sx={{ display: "inline-flex", flexDirection: "column" }}
            >
              <IconButton
                sx={{
                  p: 2,
                  fontSize: "3.5rem",
                  background: "#d4ddfc",
                  borderRadius: '50%',
                }}
              >
                <RestaurantRoundedIcon sx={{ color: "#222" }} />
              </IconButton>
              <Typography
                sx={{ fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" } }}
              >
                Food & Drink
              </Typography>
            </Grid>
            <Grid
              alignItems="center"
              sx={{ display: "inline-flex", flexDirection: "column" }}
            >
              <IconButton
                sx={{
                  p: 2,
                  fontSize: "3.5rem",
                  background: "#d4ddfc",
                  borderRadius: '50%',
                }}
              >
                <ShoppingBagRoundedIcon sx={{ color: "#222" }} />
              </IconButton>
              <Typography
                sx={{ fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" } }}
              >
                Shopping
              </Typography>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </React.Fragment>
  );
}
