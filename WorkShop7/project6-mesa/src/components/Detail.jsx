import * as React from "react";
import "./Homepage.css";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import { useTheme } from "@mui/material/styles";
import DoughnutChart from "./DoughnutChart";
import BarChart from "./BarChart";

export default function Homepage() {
  const theme = useTheme();

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="100%">
        <Box
          className="content"
          sx={{
            display: "flex",
            mx: "auto",
            p: 2,
            [theme.breakpoints.up("md")]: {
              mt: "-180px",
              flexDirection: "row",
              justifyContent: "center",
            },
            [theme.breakpoints.up("sm")]: {
              mt: "-210px",
            },
            [theme.breakpoints.up("xs")]: {
              mt: "-240px",
              flexDirection: "column",
            },
          }}
        >
          <Box
            display={"inline-flex"}
            justifyContent="center"
            sx={{
              px: 3,
              pb:2,
              [theme.breakpoints.up("md")]: {
                border: "none",
                borderRight: "#d4ddfc 2px dashed",
                alignSelf: "center"
              },
              [theme.breakpoints.up("xs")]: {
                borderBottom: "#d4ddfc 2px dashed",
              },
            }}
          >
            <Box maxWidth={{ xs: "150px", sm: "250px", md: "150x" }} sx={{}}>
              <DoughnutChart />
            </Box>

            <Box>
              <Typography
                sx={{
                  fontWeight: "bold",
                  fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" },
                }}
              >
                Selected Amount to Spent
              </Typography>
              <Typography
                sx={{
                  fontWeight: "bold",
                  fontSize: { md: "2.6rem", sm: "1.8rem", xs: "1.6rem" },
                  color: "#1E5BEB",
                }}
              >
                760 $
              </Typography>
            </Box>
          </Box>

          <Box sx={{ mr: 1, mt: 2 }}>
            <Box
              display={"flex"}
              justifyContent={"space-between"}
              sx={{ px: 2 }}
            >
              <Typography
                sx={{
                  fontWeight: "bold",
                  fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" },
                  color: "#1E5BEB",
                }}
              >
                -Spent 304
              </Typography>
              <Typography
                sx={{
                  fontWeight: "bold",
                  fontSize: { md: "1.2rem", sm: "1rem", xs: "0.7rem" },
                  color: "#33e36b",
                }}
              >
                + Left 456
              </Typography>
            </Box>
            <Box
              display={"flex"}
              sx={{ mx: "auto" }}
              maxWidth={{ xs: "300px", sm: "400px", md: "300px" }}
            >
              <BarChart />
            </Box>
          </Box>
        </Box>
      </Container>
    </React.Fragment>
  );
}
