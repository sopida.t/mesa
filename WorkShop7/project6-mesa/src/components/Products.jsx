import * as React from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import { useTheme } from "@mui/material/styles";
import { Avatar} from "@mui/material";
import Cards from './Cards'
import Loans from './Loans'
import user from '../assets/user.jpg'

export default function Products() {
  const theme = useTheme();

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="100%">
        <Box
          sx={{
            pt: 4,
            px: 4,
            height: { md: "150px", sm: "160px", xs: "120px" },
          }}
        >
          <Box display={"flex"} justifyContent={"space-between"}>
            <Box>
              <Typography
                sx={{ fontSize: { md: "1.2rem", sm: "1rem", xs: "0.8rem" } }}
              >
                My
              </Typography>
              <Typography
                fontWeight={"bold"}
                sx={{ fontSize: { md: "3rem", sm: "2.5rem", xs: "2rem" } }}
              >
                Cards
              </Typography>
            </Box>
            <Box>
              <Avatar src={user} alt='user'/>
            </Box>
          </Box>
        </Box>

        <Cards/>
        <Loans/>

        

      </Container>
    </React.Fragment>
  );
}
