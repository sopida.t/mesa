import * as React from 'react';
import { Global } from '@emotion/react';
import { styled } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import { grey } from '@mui/material/colors';
import Box from '@mui/material/Box';
import Skeleton from '@mui/material/Skeleton';
import Typography from '@mui/material/Typography';
import SwipeableDrawer from '@mui/material/SwipeableDrawer';
import './PageHome_2.css'
import { IoChevronBackOutline } from "react-icons/io5";
import Link from "@mui/material/Link"
import { TbCircleCaretRight } from "react-icons/tb";
import FormControl from '@mui/material/FormControl';
import NativeSelect from '@mui/material/NativeSelect';
import { Card, Grid } from '@mui/material';
import PicVideo1 from './pic/PicVideo1.jpg';
import PicVideo2 from './pic/PicVideo2.jpg';
import LinearProgress, { linearProgressClasses } from '@mui/material/LinearProgress';





const BorderLinearProgress = styled(LinearProgress)(({ theme }) => ({
    height: 10,
    borderRadius: 5,
    [`&.${linearProgressClasses.colorPrimary}`]: {
      backgroundColor: theme.palette.grey[theme.palette.mode === 'light' ? 200 : 800],
    },
    [`& .${linearProgressClasses.bar}`]: {
      borderRadius: 5,
      backgroundColor: theme.palette.mode === 'light' ? '#1a90ff' : '#308fe8',
    },
  }));
const drawerBleeding = 56;

const Root = styled('div')(({ theme }) => ({
  height: '100%',
  backgroundColor:
    theme.palette.mode === 'light' ? grey[100] : theme.palette.background.default,
}));

const StyledBox = styled(Box)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'light' ? '#fff' : grey[800],
}));

const Puller = styled(Box)(({ theme }) => ({
  width: 30,
  height: 6,
  backgroundColor: theme.palette.mode === 'light' ? grey[300] : grey[900],
  borderRadius: 3,
  position: 'absolute',
  top: 8,
  left: 'calc(50% - 15px)',
}));

export default function SwipeableEdgeDrawer(props) {
  const { window } = props;
  const [open, setOpen] = React.useState(false);

  const toggleDrawer = (newOpen) => () => {
    setOpen(newOpen);
  };

  // This is used only for the example
  const container = window !== undefined ? () => window().document.body : undefined;


  const [age, setAge] = React.useState('');

  
  return (
    <Root>
      <CssBaseline />
      <Global
        styles={{
          '.MuiDrawer-root > .MuiPaper-root': {
            height: `calc(30%)`,
            overflow: 'visible',
          },
        }}
      />
      <div className='headerPage2'>
        <Link><IoChevronBackOutline/></Link>
        <p>Stats</p>
        <Link><TbCircleCaretRight/></Link>
        
      </div>
        <SwipeableDrawer
            container={container}
            anchor="bottom"
            open={open}
            onClose={toggleDrawer(false)}
            onOpen={toggleDrawer(true)}
            swipeAreaWidth={drawerBleeding}
            disableSwipeToOpen={false}
            ModalProps={{
            keepMounted: true,
            }}
        >
            <StyledBox
            sx={{
                position: 'absolute',
                top: -drawerBleeding,
                borderTopLeftRadius: 30,
                borderTopRightRadius: 30,
                visibility: 'visible',
                right: 0,
                left: 0,
            }}
            >
            <Puller />
            <Typography sx={{ p: 2, color:'#333', justifyContent:'space-between',display:'flex' }}>
                <p>Video</p>
                <Box sx={{ minWidth: 10 }}>
                <FormControl  >
                    
                    <NativeSelect
                    defaultValue={30}
                    inputProps={{
                        name: 'delhi',
                        id: 'uncontrolled-native',
                        
                    }}
                    >
                    <option className="selectdetails" sx={{border:0}} value={10}>All</option>
                    <option className="selectdetails"  value={20}>video1</option>
                    </NativeSelect>
                </FormControl>
                </Box>
          </Typography>
        </StyledBox>
        <br />
        <StyledBox
          sx={{
            px: 2,
            pb: 2,
            height: '100%',
            overflow: 'auto',
          }}
        >
            
          <Skeleton variant="rectangular" height="100%" />
          <Box 
            sx={{
                flexWrap: 'wrap',
                '& > :not(style)': {
                  m: 1,
                  width: 128,
                  height: 128,
                },
              }}
          >

            <Card className='Card-container'>
              <Grid container  >
                <Grid xs ={4}>
                    <img className='picvideo' src={PicVideo1} alt="" />
                </Grid>
                <Grid className='textVideo' xs ={8}>
                    <p>48 min</p>
                    <h3>Voices and Narration</h3>
                    <BorderLinearProgress variant="determinate" value={45} />

                </Grid>
            </Grid>
            </Card>
            <Card sx={{padding:0}} className='Card-container'>
              <Grid container  >
                <Grid xs ={4}>
                <img className='picvideo' src={PicVideo2} alt="" />
                </Grid>
                <Grid className='textVideo' xs ={8}>
                    <p>52 min</p>
                    <h3>Chemical Equations</h3>
                    <BorderLinearProgress className='Progress' variant="determinate" value={70} />
                </Grid>
              </Grid>
            </Card>
            
            
          </Box>
        </StyledBox>
      </SwipeableDrawer>
    </Root>
  );
}