import { Translate } from "@mui/icons-material";
import { Box, IconButton, Typography } from "@mui/material";

export default function Recent() {
  return (
    <>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center",
          marginTop: "20px",
        }}
      >
        <Typography sx={{ fontSize: "20px", fontWeight: "bold" }}>
          Recently Played
        </Typography>
        <Typography sx={{ fontSize: "12px" }}>See all</Typography>
      </Box>
      <Box sx={{ padding: "0 0 90px 0" }}>
        <Box
          sx={{
            boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
            width: "90vw",
            height: "9vh",
            marginTop: "10px",
            borderRadius: "10px",
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <Box
                sx={{
                  width: "50px",
                  height: "50px",
                  bgcolor: "#fff",
                  m: 2,
                  backgroundImage:
                    "url(https://i.scdn.co/image/ab67616d0000b273a1edbe4e3f3e3fe296816af4)",
                  backgroundSize: "cover",
                  borderRadius: "10px",
                }}
              />
              <Box>
                <Typography>Mirror ft.Bruno Mars</Typography>
                <Typography>By Lil Wayne</Typography>
              </Box>
            </Box>
            <Box sx={{ transform: "translate(0,-10%)" }}>
              <IconButton>...</IconButton>
            </Box>
          </Box>
        </Box>

        <Box
          sx={{
            boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
            width: "90vw",
            height: "9vh",
            marginTop: "10px",
            borderRadius: "10px",
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <Box
                sx={{
                  width: "50px",
                  height: "50px",
                  bgcolor: "#fff",
                  m: 2,
                  backgroundImage:
                    "url(https://i.scdn.co/image/ab67616d0000b273a1edbe4e3f3e3fe296816af4)",
                  backgroundSize: "cover",
                  borderRadius: "10px",
                }}
              />
              <Box>
                <Typography>Mirror ft.Bruno Mars</Typography>
                <Typography>By Lil Wayne</Typography>
              </Box>
            </Box>
            <Box sx={{ transform: "translate(0,-10%)" }}>
              <IconButton>...</IconButton>
            </Box>
          </Box>
        </Box>
        <Box
          sx={{
            boxShadow: "rgba(0, 0, 0, 0.15) 1.95px 1.95px 2.6px",
            width: "90vw",
            height: "9vh",
            marginTop: "10px",
            borderRadius: "10px",
            display: "flex",
            alignItems: "center",
          }}
        >
          <Box
            sx={{
              display: "flex",
              alignItems: "center",
              justifyContent: "space-between",
              width: "100%",
            }}
          >
            <Box sx={{ display: "flex", alignItems: "center" }}>
              <Box
                sx={{
                  width: "50px",
                  height: "50px",
                  bgcolor: "#fff",
                  m: 2,
                  backgroundImage:
                    "url(https://i.scdn.co/image/ab67616d0000b273a1edbe4e3f3e3fe296816af4)",
                  backgroundSize: "cover",
                  borderRadius: "10px",
                }}
              />
              <Box>
                <Typography>Mirror ft.Bruno Mars</Typography>
                <Typography>By Lil Wayne</Typography>
              </Box>
            </Box>
            <Box sx={{ transform: "translate(0,-10%)" }}>
              <IconButton>...</IconButton>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
}
