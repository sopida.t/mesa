import { useState } from 'react'
import Home from './components/Home'
import Task from './components/Task'
import { Route, Routes, BrowserRouter } from "react-router-dom";
function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="task" element={<Task />} />
    </Routes>
  </BrowserRouter>
  )
}

export default App
