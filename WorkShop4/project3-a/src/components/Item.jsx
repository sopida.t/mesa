import React from "react";
import "../components/Item.css";
import { useCartContext } from "../context/Context";

const Item = ({ data }) => {
  const { formatMoney, removeItem, increaseItem, decreaseItem, total } =
    useCartContext();
  return (
    <div className="cart">
      <img src={data.image} alt={data.id} />
      <div className="product">
        <p className="name">ชื่อสินค้า : {data.name}</p>
        <p className="price">ราคาสอนค้า : {formatMoney(data.price)}</p>
      </div>
      <div className="quantity">
        <button onClick={() => increaseItem(data.id)}>+</button>
        <input type="text" value={data.quantity} disabled />
        <button onClick={() => decreaseItem(data.id)}>-</button>
      </div>
      <div className="total-price">
        ยอดรวม {formatMoney(data.quantity * data.price)}
      </div>
      <div>
        <button onClick={() => removeItem(data.id)}>ลบสินค้า</button>
      </div>
    </div>
  );
};

export default Item;
