import React from "react";
import "./Header.css";
import { useCart } from "../context/Cartcontext";

export default function Header() {
  const { amount } = useCart();
  return (
    <header>
      <p>Shop Application</p>
      <p>ตะกร้าสินค้า : {amount}</p>
    </header>
  );
}
