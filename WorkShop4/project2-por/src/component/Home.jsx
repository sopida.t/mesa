import picHome from "../pic/Home.png";
export default function Home(){
    return(
        <div className="container">
            <h2 className="title">หน้าแรกของเว็บไซต์</h2>
            <img src={picHome} alt="home" />
        </div>

    );
}