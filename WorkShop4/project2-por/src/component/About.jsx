import about from "../pic/about.png";
export default function About(){
    return(
        <div className="container">
            <h2 className="title">เกี่ยวกับเรา</h2>
            <img src={about} alt="about" />
        </div>
    );
}