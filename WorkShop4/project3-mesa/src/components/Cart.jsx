import Item from "./Item";
import { useCart } from "../context/CartContext";
import "./Cart.css";

export default function Cart() {
  const { products, total, formatMoney } = useCart();

  return (
    <div className="cart">
      {products.map((data) => {
        return <Item key={data.id} item={data} {...data} />;
      })}
      <div style={{ textAlign: products.length > 0 ? "right" : "center" }} className="total-price">
        {products.length > 0 ? (
          <h2 className="with-products">ยอดรวม : {formatMoney(total)} บาท</h2>
        ) : (
          <h2 className="without-products">ไม่มีสินค้าในตะกร้า</h2>
        )}
      </div>
    </div>
  );
}
