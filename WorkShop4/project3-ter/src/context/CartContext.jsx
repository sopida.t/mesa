import { createContext, useContext, useReducer } from "react";
import products from "../data/products";
import cartReducer from "../reducer/Cartreducer";
import { useEffect } from "react";
const CartContext = createContext();
const initState = {
  products: products,
  total: 0,
  amount: 0,
};
export const CartProvider = ({ children }) => {
  const [state, dispatch] = useReducer(cartReducer, initState);
  function formatMoney(money) {
    return money.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,");
  }
  function addQuantity(id) {
    dispatch({ type: "ADD", payload: id });
    console.log("เพิ่มปริมาณสินค้า = " + id);
  }
  function subtractQuantity(id){
    dispatch({ type: "SUBTRACT", payload: id })
    console.log("ลดปริมาณสินค้า = " + id);
  }
  function removeItem(id) {
    dispatch({ type: "REMOVE", payload: id });
    console.log("ลบสินค้า = " + id);
  }
  useEffect(() => {
    console.log("หาผลรวม");
    dispatch({ type: "CALCULATE_TOTAL" });
  }, [state.products]);
  return (
    <CartContext.Provider
      value={{ ...state, formatMoney, removeItem, addQuantity, subtractQuantity }}
    >
      {children}
    </CartContext.Provider>
  );
};
export const useCart = () => {
  return useContext(CartContext);
};
