import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import "./Details.css"
import blogs from "../data/blogs";
export default function Details() {
  const { id } = useParams();
  const [title, setTitle] = useState("");
  const [img, setImg] = useState("");
  const [content, setContent] = useState("");
  const [author, setAuthor] = useState("");
  useEffect(() => {
    const result = blogs.find((item) => item.id === parseInt(id));
    setTitle(result.title);
    setImg(result.image_url);
    setContent(result.content);
    setAuthor(result.author);
  }, []);
  return (
    <>
      <div className="container">
        <h2 className="title"> บทความ : {title}</h2>
        <img src={img} alt={title} />
        <div className="blog-detail">
          <h3><strong>ผู้เขียน : {author}</strong></h3>
          <p>{content}</p>
        </div>
      </div>
    </>
  );
}
