import home from "../images/home.png";
export default function Home() {
  return (
    <div className="container">
      <h1 className="title">หนัาเเรก</h1>
      <img src={home} alt="home" />
    </div>
  );
}
