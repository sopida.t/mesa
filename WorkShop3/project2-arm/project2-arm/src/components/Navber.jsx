import { Link } from "react-router-dom";
import "./Navber.css";
export default function Navber() {
  return (
    <nav>
      <Link to="/" className="logo">
        <h1>Blog App</h1>
      </Link>
      <Link to="/">หน้าเเรก</Link>
      <Link to="/blogs">บทความ</Link>
      <Link to="/about">เกี่ยวกับเรา</Link>
    </nav>
  );
}
