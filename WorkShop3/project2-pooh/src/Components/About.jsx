import about from "../Images/about.jpg";

export default function About() {
  return (
    <div className="container">
      <h2>All topics</h2>
      <img src={about} alt="about" />
    </div>
  );
}
