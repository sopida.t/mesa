import { useState } from "react";
import "./App.css";
import Home from "./Components/Home";
import About from "./Components/About";
import Blogs from "./Components/Blog";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import Navbar from "./Components/Navbar";
import NotFound from "./Components/NotFound";
import Details from "./Components/Detail";
function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="/about" element={<About />}></Route>
        <Route path="/blogs" element={<Blogs />}></Route>
        <Route path="*" element={<NotFound />}></Route>
        <Route path="/home" element={<Navigate to="/" />}></Route>
        <Route path="blog/:id" element={<Details />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
