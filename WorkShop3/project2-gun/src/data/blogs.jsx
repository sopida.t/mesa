const blogs = [
    {
      id: 1,
      title: "a1",
      image_url:
        "https://img.freepik.com/premium-photo/image-colorful-galaxy-sky-generative-ai_791316-9864.jpg?w=1800",
      content: "หูกระจงควรปลูกให้ห่างจากตัวบ้าน1",
      author: "Poohdude1",
    },
    {
      id: 2,
      title: "b",
      image_url:
        "https://img.freepik.com/premium-photo/image-colorful-galaxy-sky-generative-ai_791316-9864.jpg?w=1800",
      content: "หูกระจงควรปลูกให้ห่างจากตัวบ้าน2",
      author: "Poohdude2",
    },
    {
      id: 3,
      title: "c",
      image_url:
        "https://img.freepik.com/premium-photo/image-colorful-galaxy-sky-generative-ai_791316-9864.jpg?w=1800",
      content: "หูกระจงควรปลูกให้ห่างจากตัวบ้าน3",
      author: "Poohdude3",
    },
    {
      id: 4,
      title: "d",
      image_url:
        "https://img.freepik.com/premium-photo/image-colorful-galaxy-sky-generative-ai_791316-9864.jpg?w=1800",
      content: "หูกระจงควรปลูกให้ห่างจากตัวบ้าน4",
      author: "Poohdude4",
    },
    {
      id: 5,
      title: "e",
      image_url:
        "https://img.freepik.com/premium-photo/image-colorful-galaxy-sky-generative-ai_791316-9864.jpg?w=1800",
      content: "หูกระจงควรปลูกให้ห่างจากตัวบ้าน5",
      author: "Poohdude5",
    },
  ];

  export default blogs;