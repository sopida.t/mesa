import blogs from "../data/blogs.jsx";
import "./Blogs.css";
import { Link } from "react-router-dom";
import { useState } from "react";
import { useEffect } from "react";

export default function Blogs() {
  const [search, setSearch] = useState("");
  const [filterBlog, setFilterBlog]= useState([])

  useEffect(()=>{
    //กรองข้อมูลข้อความ
    const result = blogs.filter((item)=>item.title.includes(search))
    setFilterBlog(result)
    console.log(result)
  },[search])
  
  return (
    <div className="blogs-container">
      <div className="search-container">
        <input
          type="text"
          className="search-input"
          placeholder="ค้นหาบทความ"
          value={search}
          onChange={(e)=>setSearch(e.target.value)}
        ></input>
      </div>

      <article>
        {filterBlog.map((item) => (
          <Link to={`/blogs/${item.id}`} key={item.id}>
            <div className="card">
              <h2>{item.title}</h2>
              <p>{item.content.substring(0, 300)}</p>
              <hr></hr>
            </div>
          </Link>
        ))}
      </article>
    </div>
  );
}
