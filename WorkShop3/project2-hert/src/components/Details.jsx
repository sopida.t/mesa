import { useEffect } from "react";
import { useParams } from "react-router-dom";
import blogs from "../data/blogs";
import { useState } from "react";
import './Details.css'

export default function Details() {
  const { id } = useParams();
  const [title, setTitle] = useState("");
  const [image, setImage] = useState("");
  const [content, setContent] = useState("");
  const [author, setAuthur] = useState("");

  useEffect(() => {
    //ดึงข้อมูลบทความ
    const result = blogs.find((item) => item.id === parseInt(id));
    console.log(result);
    setTitle(result.title);
    setImage(result.image_url);
    setContent(result.content);
    setAuthur(result.author);
    // eslint-disable-next-line
  }, []);
  return (
    <div className="container">
      <h2 className="title">บทความ : {title}</h2>
      <img src={image} alt={title} className="blog-image"></img>
      <div className="blog-detail">
        <strong>ผู้เขียน : {author}</strong>
        <p>{content}</p>
      </div>
    </div>
  );
}
