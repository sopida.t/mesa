import Workshop1 from "./Workshop/ws1/Workshop1";
import Workshop2 from "./Workshop/ws2/Workshop2";
// import Header from "./Components/Header";
// import StudentList from "./Components/stdls";
import { useState } from "react";
// import AddForm from "./Components/addForm";


// import { BrowserRouter, Routes, Route } from "react-router-dom";

function App() {
  const [students, setStudent] = useState([
  ]);

  function deleteStudent(id) {
    setStudent(students.filter((item) => item.id !== id));
  }

  return (
    <>
      <Workshop1 />
      <Workshop2 />
      {/* <Header title="Home" />
      <main>
        <AddForm students={students} setStudent={setStudent}/>
        <StudentList students={students} deleteStudent={deleteStudent} />
      </main> */}
    </>
  );
}

export default App;
