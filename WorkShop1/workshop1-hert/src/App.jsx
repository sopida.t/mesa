import { useState } from 'react'
import HertImg from './assets/Hert.jpg'
import viteLogo from '/vite.svg'
import './App.css'

function App() {
  const [count, setCount] = useState(0)

  return (
    <div className='container'>

      <div className='ImgContainer'>
        <h1 className='ContentTextStyle'>RESUME</h1>
        <div className='ImgStyle'>
          <img src={HertImg} alt="Hert"></img>
        </div>
      </div>
      
      <div className='NameContainer'>
        <div className='NameBox'>
          <h1 className='TextNameStyle'>DECHANUPHAP ANUWAN</h1>
        </div>
      </div>

      <div>
        <div className='ContainerDetailText'>


            <div className='DetailBox'>
              <div className='DetailRow'>

                <div>
                  <div>
                  <p>Nickname : Hert</p>
                  <p>Position : Front-end</p>
                  <p>Skill :</p>
                    <ul className='DetailText'>
                      <li>HTML</li>
                      <li>CSS</li>
                  </ul>
                  <p>Hobby :</p>
                    <ul className='DetailText'>
                      <li>Coding</li>
                      <li>Sing</li>
                      <li>Dance</li>
                      <li>Exercise</li>
                    </ul>
                    </div>
                </div>

                  <div>
                    <div className='DetailLine'></div>
                  </div>

                  <div>
                    <p className='MottoTextStyle'>"Hello World!"</p>
                  </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  )
}

export default App
