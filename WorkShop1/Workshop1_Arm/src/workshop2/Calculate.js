import React, { useState } from "react";

function Calculator() {
  const [num1, setNum1] = useState("");
  const [num2, setNum2] = useState("");
  const [operator, setOperator] = useState("+");

  const handleNum1Change = (event) => {
    setNum1(event.target.value);
  };

  const handleNum2Change = (event) => {
    setNum2(event.target.value);
  };

  const handleOperatorChange = (event) => {
    setOperator(event.target.value);
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    let result;

    switch (operator) {
      case "+":
        result = parseInt(num1) + parseInt(num2);
        break;
      case "-":
        result = parseInt(num1) - parseInt(num2);
        break;
      case "*":
        result = parseInt(num1) * parseInt(num2);
        break;
      case "/":
        result = parseInt(num1) / parseInt(num2);
        break;
      default:
        result = "Invalid operator";
    }
    alert("Answer = " + result);
  };

  return (
    <div
      style={{
        position: "absolute",
        top: "20%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        marginTop: "100px",
        width: "50%",
        height: "200px",
        border: "3px solid black",
        borderRadius: "30px"
      }}
    >
      <div
        style={{
          textAlign: "center",
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        <form onSubmit={handleSubmit}>
          <label>
            Number 1 |
            <input
              type="number"
              value={num1}
              onChange={handleNum1Change}
              style={{ width: "20px" }}
            />
          </label>
          <br />
          <label>
            Number 2 |
            <input
              type="number"
              value={num2}
              onChange={handleNum2Change}
              style={{ width: "20px" }}
            />
          </label>
          <br />
          <label>
            Operator |
            <select value={operator} onChange={handleOperatorChange}>
              <option value="+">+</option>
              <option value="-">-</option>
              <option value="*">*</option>
              <option value="/">/</option>
            </select>
          </label>
          <br />
          <button type="submit" style={{ position: "relative",
        top: "20px", }}>Calculate</button>
        </form>
      </div>
    </div>
  );
}

export default Calculator;
