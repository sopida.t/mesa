import "./App.css";
import MyHeader from "./assets/components/MyHeader";
import MyCalendar from "./assets/components/MyCalendar";
import Content from "./assets/components/Content";
import MyNavbar from "./assets/components/MyNavbar";

function App() {
  return (
    <>
      <MyHeader />
      <MyCalendar/>
      <Content />
      <MyNavbar />
    </>
  );
}

export default App;
